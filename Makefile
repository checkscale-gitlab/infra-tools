# -----------------------------------------------------------------------------
#  MAKEFILE RUNNING COMMAND
# -----------------------------------------------------------------------------
#  Author     : Dwi Fahni Denni (@zeroc0d3)
#  Repository : https://gitlab.com/zeroc0d3lab-devops-poc/infra-tools
#  License    : Apache v2
# -----------------------------------------------------------------------------
# Notes:
# use [TAB] instead [SPACE]


.PHONY: run stop remove build push push-container

export PATH_WORKSPACE="src"
export PATH_COMPOSE="compose"
export PATH_DOCKER="compose/docker"
export PATH_DOCKUBE="dockube"
export PATH_KUBESPRAY="workspace/kubespray"
export PATH_TERRAFORM="terraform"
export PATH_TERRAGRUNT="terragrunt"
export PROJECT_NAME="infra-tools"

export CI_REGISTRY     ?= asia.gcr.io/zeroc0d3lab-devops-poc
export CI_PROJECT_PATH ?= deployment

IMAGE          = $(CI_REGISTRY)/${CI_PROJECT_PATH}/infra-tools
DIR            = $(shell pwd)
VERSION       ?= 1.3.0

BASE_IMAGE     = golang
BASE_VERSION   = 1.14.7-alpine3.12

#------------------------
# Container Builder
#------------------------
run:
	@echo "============================================"
	@echo " Task      : Docker Container "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@./run-docker.sh
	@echo '- DONE -'

stop:
	@echo "============================================"
	@echo " Task      : Stopping Docker Container "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@docker-compose -f ${PATH_COMPOSE}/app-compose.yml stop
	@echo '- DONE -'

remove:
	@echo "============================================"
	@echo " Task      : Remove Docker Container "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@docker-compose -f ${PATH_COMPOSE}/app-compose.yml down
	@echo '- DONE -'

build:
	@echo "============================================"
	@echo " Task      : Create Docker Image "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_DOCKER} && ./docker-build.sh
	@echo '- DONE -'

push:
	@echo "============================================"
	@echo " Task      : Push Docker Image "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_DOCKER} && ./docker-push.sh
	@echo '- DONE -'

push-container:
	@echo "============================================"
	@echo " Task      : Push Docker Hub "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_DOCKER} && ./docker-build.sh
	@cd ${PATH_DOCKER} && ./docker-push.sh
	@echo '- DONE -'


#------------------------
# Terraform Services
#------------------------
trf-init:
	@echo "============================================"
	@echo " Task      : Terraform Init "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terraform init

trf-apply:
	@echo "============================================"
	@echo " Task      : Terraform Apply "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terraform apply

trf-destroy:
	@echo "============================================"
	@echo " Task      : Terraform Destroy "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terraform destroy

trf-get:
	@echo "============================================"
	@echo " Task      : Terraform Get "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terraform get

trf-plan:
	@echo "============================================"
	@echo " Task      : Terraform Plan "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terraform plan

trf-wrk-list:
	@echo "============================================"
	@echo " Task      : Terraform Workspace List "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terraform workspace list

trf-wrk-select:
	@echo "============================================"
	@echo " Task      : Terraform Workspace Select "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terraform workspace select $1

trf-wrk-new:
	@echo "============================================"
	@echo " Task      : Terraform Workspace New "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terraform workspace new $1


#------------------------
# Terragrunt Services
#------------------------
trg-apply:
	@echo "============================================"
	@echo " Task      : Terragrunt Apply "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terragrunt apply

trg-destroy:
	@echo "============================================"
	@echo " Task      : Terragrunt Destroy "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terragrunt destroy

trg-get:
	@echo "============================================"
	@echo " Task      : Terragrunt Get "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terragrunt get

trg-output:
	@echo "============================================"
	@echo " Task      : Terragrunt Output "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terragrunt output

trg-plan:
	@echo "============================================"
	@echo " Task      : Terragrunt Plan "
	@echo " Date/Time : `date`"
	@echo "============================================"
	@cd ${PATH_TERRAFORM} && terragrunt plan
