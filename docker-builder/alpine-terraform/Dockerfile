FROM alpine:3.12

ARG BUILD_DATE
ARG BUILD_VERSION
ARG GIT_COMMIT
ARG GIT_URL

LABEL maintainer="zeroc0d3.team@gmail.com" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.name="alpine-terraform" \
      org.label-schema.description="alpine-terraform image" \
      org.label-schema.vcs-ref="$GIT_COMMIT" \
      org.label-schema.vcs-url="$GIT_URL" \
      org.label-schema.vendor="ZeroC0D3Lab" \
      org.label-schema.version="$BUILD_VERSION" \
      org.label-schema.schema-version="1.0"

ENV TERRAFORM_VERSION=0.12.29

RUN apk add --no-cache --update curl zip && \
    curl -sSL https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
        -o /tmp/terraform.zip && \
    unzip /tmp/terraform.zip -d /usr/bin && \
    rm /tmp/terraform.zip && \
    apk del curl zip

ENTRYPOINT ["/usr/bin/terraform"]
CMD []

VOLUME ["/data"]
WORKDIR /data
