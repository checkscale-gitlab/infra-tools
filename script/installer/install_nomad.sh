#!/usr/bin/env sh

# ================================================================================================
#  INSTALL VAULT
# ================================================================================================
export DEBIAN_FRONTEND=noninteractive
export NOMAD_VERSION="0.12.3"

if ! [ "${ND_VERSION}" = "" ]
then
  NOMAD_VERSION=${ND_VERSION}
fi

apt-get update
apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install \
  git \
  bash \
  curl \
  wget \
  zip \
  unzip \
  software-properties-common \
  openssh-server \
  openssh-client

wget -nv https://releases.hashicorp.com/nomad/${NOMAD_VERSION}/nomad_${NOMAD_VERSION}_linux_amd64.zip -O /tmp/nomad_${NOMAD_VERSION}_linux_amd64.zip

cd /tmp
unzip -o nomad_${NOMAD_VERSION}_linux_amd64.zip
chmod +x /tmp/nomad

mv /tmp/vault /usr/local/bin/nomad
